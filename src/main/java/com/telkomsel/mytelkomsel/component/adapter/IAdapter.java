package com.telkomsel.mytelkomsel.component.adapter;

import java.util.List;

public interface IAdapter<T> {
    List<T> getDisplayItems();
    void refresh();
}
