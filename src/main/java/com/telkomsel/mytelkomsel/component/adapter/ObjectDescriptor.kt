package com.telkomsel.mytelkomsel.component.adapter

import com.telkomsel.mytelkomsel.core.modules.ModuleUtils
import java.lang.Exception

class ObjectDescriptor {

    companion object {

        fun printLog(log: String?) { }

    }

    var type: Class<*>? = null
    var json: String? = null
    var instance: Any? = null

    constructor(type: Class<*>): this(type, null)
    constructor(type: Class<*>, json: String?) {
        this.type = type
        this.json = json

        init()
    }

    private fun init() {
        try {
            instance = if(json != null) {
                printLog("create from json : $type")
                ModuleUtils.fromJson(json, type)
            } else {
                printLog("create from class : $type")
                type?.newInstance()
            }
        } catch(exc: Exception) {
            exc.printStackTrace()
            printLog("Exception caught on init : " + exc.message)
        }
    }

}