package com.telkomsel.mytelkomsel.component.adapter

import android.content.Context
import android.view.View
import com.telkomsel.mytelkomsel.component.R
import com.telkomsel.mytelkomsel.component.databinding.BottomSheetListItemBinding
import com.telkomsel.mytelkomsel.component.model.BottomSheetData

class EmbededBottomSheetAdapter(context: Context?, items: List<BottomSheetData?>?) :
        BaseAdapter<BottomSheetData, EmbededBottomSheetAdapter.BottomPurchaseVH>(context, items) {

    override fun createViewHolder(view: View): BottomPurchaseVH? {
        val binding = BottomSheetListItemBinding.bind(view)
        return BottomPurchaseVH(binding)
    }

    override fun bindView(holder: BottomPurchaseVH, item: BottomSheetData?, position: Int) {
        holder.bindView(item)
    }

    inner class BottomPurchaseVH(binding: BottomSheetListItemBinding) : BaseViewHolder<BottomSheetData?>(binding.root) {
        val tvTitle = binding.tvPurchaseDetailTitle
        private val tvPrice = binding.tvPurchaseDetailPrice

        public override fun bindView(item: BottomSheetData?) {
            tvTitle.text = item?.title
            tvPrice.text = item?.price
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.bottom_sheet_list_item
    }
}