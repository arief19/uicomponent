package com.telkomsel.mytelkomsel.component.adapter;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.ButterKnife;

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    private Context context;

    public BaseViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        context = view.getContext();
    }

    protected abstract void bindView(T item);

    protected Context getContext() {
        return context;
    }

}
