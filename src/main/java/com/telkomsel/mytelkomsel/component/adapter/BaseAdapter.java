package com.telkomsel.mytelkomsel.component.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> implements IAdapter<T> {

    private final Context mContext;
    private final LayoutInflater mInflater;
    private OnItemClickListener mListener;
    private OnClickListener<T> onClickListener;
    private List<T> mDisplayItems;
    private int maxSize = 0;

    public BaseAdapter(Context context, List<T> items) {
        mContext = context;
        mDisplayItems = items;
        if (mDisplayItems == null) {
            mDisplayItems = new ArrayList<>();
        }
        mInflater = LayoutInflater.from(context);
        this.maxSize = 0;
    }

    protected abstract int getLayoutId();

    protected abstract VH createViewHolder(View view);

    protected abstract void bindView(VH holder, T item, int position);

    public final void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public final void setOnClickListener(OnClickListener<T> listener) { this.onClickListener = listener; }

    public final Context getContext() {
        return mContext;
    }

    public int getMaxSize() { return maxSize; }
    public void setMaxSize(int maxSize) { this.maxSize = maxSize; }

    public final LayoutInflater getLayoutInflater() {
        return mInflater;
    }

    public final List<T> getDisplayItems() {
        return mDisplayItems;
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = getLayoutInflater().inflate(getLayoutId(), parent, false);
        return createViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VH holder, @SuppressLint("RecyclerView") final int position) {
        T item = getItemAtPosition(position);
        if (item != null) {
            bindView((VH) holder, item, position);
        }
        holder.itemView.setOnClickListener(v -> {
            onItemClicked(v, item, position);
        });
    }

    protected void onItemClicked(View v, T item, int position) {
        if (mListener != null) {
            mListener.onItemClick(BaseAdapter.this, v, position);
        }
        if(onClickListener != null) {
            onClickListener.onClick(this, v, item, position);
        }
    }

    @Override
    public int getItemCount() {
		int count = 0;
        if(mDisplayItems == null) {
			count = 0;
		} else {
        	count = (maxSize > 0 && mDisplayItems.size() > maxSize) ? maxSize : mDisplayItems.size();
		}
        return count;
    }

    public final T getItemAtPosition(int position) {
        if(mDisplayItems == null || mDisplayItems.size() <= position) return null;
        return mDisplayItems.get(position);
    }

    protected void onItemClick(VH holder, int position) {

    }

    public final void addData(List<T> items) {
        if (mDisplayItems == null) {
            mDisplayItems = new ArrayList<>();
        }
        mDisplayItems.addAll(items);
        notifyDataSetChanged();
    }

    public final void updateData(List<T> items) {
		if(mDisplayItems != null) {
			mDisplayItems.clear();
		}
		addData(items);
    }

    public String getAdapterName() {
        return this.getClass().getSimpleName();
    }

    public interface OnItemClickListener {
        void onItemClick(BaseAdapter adapter, View view, int position);
    }

	public interface OnClickListener<T> {
		void onClick(BaseAdapter adapter, View view, T item, int position);
	}

}
