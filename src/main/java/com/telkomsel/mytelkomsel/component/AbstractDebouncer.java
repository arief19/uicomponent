package com.telkomsel.mytelkomsel.component;

import android.os.Handler;
import android.os.Looper;

public abstract class AbstractDebouncer<T extends AbstractDebouncer<T>> {

    /**
     * RESET : will kill the executing thread whenever the trigger happen
     * EXACT : will execute on specified delay (delay > 0) regardless the new trigger is happen
     */
    public enum DebounceType { RESET, EXACT }

    private DebounceType type = DebounceType.EXACT;
    private OnPreExecuteListener preListener = null;
    private OnExecuteListener listener = null;
    private OnPostExecuteListener postListener = null;
    private int delay = 0;
    private boolean executing = false;
    private boolean initializing = false;
    private Thread executeThread = null;

    protected AbstractDebouncer() { }

    public T build() {
        initializing = true;
        initialize();
        initializing = false;
        return (T) this;
    }

    protected abstract void initialize();

    public T setType(DebounceType type) {
        this.type = type;
        return (T) this;
    }

    public T setDelay(int delay) {
        this.delay = delay;
        return (T) this;
    }

    public T setOnPreListener(OnPreExecuteListener listener) {
        this.preListener = listener;
        return (T) this;
    }

    public T setOnExecuteListener(OnExecuteListener listener) {
        this.listener = listener;
        return (T) this;
    }

    public T setOnPostExecuteListener(OnPostExecuteListener listener) {
        this.postListener = listener;
        return (T) this;
    }

    protected void execute() {
        if(initializing) return;

        printLog("executing : " + executing + ", type : " + type);
        fireOnPreExecute();
        if(type == DebounceType.RESET) {
            executeReset();
        } else {
            executeExact();
        }
    }

    public void cancel() {
        if(executeThread == null) return;
        executeThread.interrupt();
    }

    private void executeReset() {
        cancel();
        createThread();
    }

    private void executeExact() {
        if (executing) return;
        executing = true;
        createThread();
    }

    private void createThread() {
        printLog("createThread");
        executeThread = new Thread(() -> {
            try {
                new Handler(Looper.getMainLooper()).post(this::fireOnExecute);
                Thread.sleep(delay);
                printLog("onSleepFinished");
                if(executeThread.isInterrupted()) return;
                new Handler(Looper.getMainLooper()).post(() -> {
                    fireOnPostExecute();
                    executing = false;
                });
            }
            catch(Exception exc) {
                printLog("Exception caught on Debouncer : " + exc);
            }
        });
        executeThread.start();
    }

    protected void printLog(String log) {
        System.out.println("#DEBOUNCER#." + log);
    }

    private void fireOnPreExecute() {
        if(preListener == null) return;
        preListener.onPreExecute();
    }

    private void fireOnExecute() {
        if(listener == null) return;
        listener.onExecute();
    }

    private void fireOnPostExecute() {
        if(postListener == null) return;
        postListener.onPostExecute();
    }

    public interface OnPreExecuteListener {
        void onPreExecute();
    }

    public interface OnExecuteListener {
        void onExecute();
    }

    public interface OnPostExecuteListener {
        void onPostExecute();
    }

}
