package com.telkomsel.mytelkomsel.component.button

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.Gravity
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatButton
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.telkomsel.mytelkomsel.component.CpnUtils
import com.telkomsel.mytelkomsel.component.Font
import com.telkomsel.mytelkomsel.component.R

open class PrimaryButton : AppCompatButton {

    enum class Position { NONE, LEFT, TOP, RIGHT, BOTTOM }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.PrimaryButton)

        initBackground(context, typedArray)
        initFont(context, typedArray)
        initIcon(context, typedArray)
        initGravity(typedArray)

        typedArray.recycle()
    }

    private fun printLog(log: String) { }

    protected open fun initBackground(context: Context, typedArray: TypedArray) {
        val background = CpnUtils.getDrawable(context, typedArray, R.styleable.PrimaryButton_android_background, getDefaultBackground())

        printLog("initBackground[background : $background]")
        setBackground(background)
    }

    protected open fun getDefaultBackground(): Int {
        return R.drawable.primary_button
    }

    protected open fun initIcon(context: Context, typedArray: TypedArray) {
        val icon = CpnUtils.getDrawable(context, typedArray, R.styleable.PrimaryButton_icon, getDefaultIcon())

        val pos = typedArray.getInt(R.styleable.PrimaryButton_iconPosition, -1)
        var position = getDefaultIconPosition()
        val positions = Position.values()
        if(pos != -1 && pos < positions.size) {
            position = positions[pos]
        }

        var tint: ColorStateList? = null;
        if(typedArray.hasValue(R.styleable.PrimaryButton_android_tint)) {
            tint = CpnUtils.getColorList(context, typedArray, R.styleable.PrimaryButton_android_tint, Color.BLACK)
            icon?.setTintList(tint)
        } else {
            icon?.setTint(currentTextColor)
        }
        printLog("initIcon[icon : $icon, position : $position]")
        setIcon(icon, position)
    }

    protected open fun getDefaultIcon(): Int {
        return R.drawable.default_icon
    }

    protected open fun getDefaultIconPosition(): Position {
        return Position.NONE
    }

    fun removeIcon() {
        setIcon(null, Position.NONE)
    }

    fun setIcon(icon: Drawable?, position: Position) {
        icon?.setBounds(5, 5, 5, 5)
        setCompoundDrawablesWithIntrinsicBounds(
            if(position == Position.LEFT) icon else null,
            if(position == Position.TOP) icon else null,
            if(position == Position.RIGHT) icon else null,
            if(position == Position.BOTTOM) icon else null
        )
    }

    fun setIconResource(imageUrl: String?, errorIcon: Drawable) {
        var icon: Drawable?
        val ivIcon: ImageView? = null

        Glide.with(this)
                .load(imageUrl)
                .error(errorIcon)
                .into(object : CustomTarget<Drawable?>() {
                    override fun onLoadCleared(placeholder: Drawable?) {

                    }

                    override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable?>?) {
                        icon = resource
                        ivIcon?.setImageDrawable(resource)
                    }

                })
    }

    private fun initFont(context: Context, typedArray: TypedArray) {
        try {
            val builder = Font.Builder(this)
                .setFamily(R.styleable.PrimaryButton_android_fontFamily, R.font.poppins_bold)
                .setFontStyle(R.styleable.PrimaryButton_android_textStyle, Typeface.BOLD)
                .setFontSize(R.styleable.PrimaryButton_android_textSize, R.dimen._12ssp)
                .setFontColor(R.styleable.PrimaryButton_android_textColor, R.color.primary_button_text)
                .setAllCaps(R.styleable.PrimaryButton_android_textAllCaps, false)
                .setIsUnderline(R.styleable.PrimaryButton_textUnderline, false)
            initFont(builder, context, typedArray)
            builder.build(context, typedArray)

        } catch(exc: Exception) {
            exc.printStackTrace()
            printLog("Exception caught on initFont : " + exc.message)
        }
    }

    protected open fun initFont(builder: Font.Builder, context: Context, typedArray: TypedArray) { }

    protected open fun initGravity(typedArray: TypedArray) {
        val gravity = typedArray.getInt(R.styleable.PrimaryButton_android_gravity, getDefaultGravity())

        printLog("initGravity[gravity : $gravity]")
        setGravity(gravity)
    }

    protected open fun getDefaultGravity(): Int {
        return Gravity.CENTER
    }

}
