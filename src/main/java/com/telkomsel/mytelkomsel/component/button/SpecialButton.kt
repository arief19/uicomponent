package com.telkomsel.mytelkomsel.component.button

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.telkomsel.mytelkomsel.component.CpnUtils
import com.telkomsel.mytelkomsel.component.Font
import com.telkomsel.mytelkomsel.component.R
import java.lang.Exception

open class SpecialButton : LinearLayout {

    var container: View? = null
        private set
    var icon: IconButton? = null
        private set
    var text: TextView? = null
        private set
    var value: TextView? = null
        private set

    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init(context, attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr) {
        init(context, attributeSet)
    }

    private fun printLog(log: String) { }

    private fun init(context: Context, attributeSet: AttributeSet) {
        container = LayoutInflater.from(context).inflate(R.layout.special_button, this, false)
        addView(container)
        printLog("init[$container]")

        val typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.SpecialButton)

        initBackground(context, typedArray)
        initFont(context, typedArray)
        initIcon(context, typedArray)
        initPadding()

        typedArray.recycle()

        onInitialized()
    }

    protected open fun onInitialized() { }

    private fun initPadding() {
        container?.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom)
        container?.layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        setPadding(0, 0, 0, 0)
        printLog("size[${layoutParams.width}, ${layoutParams.height}]" +
                ".padding[$paddingStart, $paddingTop, $paddingEnd, $paddingBottom]")
        printLog("container.size[${container?.layoutParams?.width}, ${container?.layoutParams?.height}]" +
                ".padding[${container?.paddingStart}, ${container?.paddingTop}, ${container?.paddingEnd}, ${container?.paddingBottom}]")
    }

    final override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        container?.isEnabled = enabled
        icon?.isEnabled = enabled
        text?.isEnabled = enabled
    }

    final override fun setOnClickListener(l: OnClickListener?) {
        container?.setOnClickListener(l)
    }

    private fun initBackground(context: Context, typedArray: TypedArray) {
        try {
            container ?: return
            val background: Drawable? = CpnUtils.getColorOrDrawable(context, typedArray, R.styleable.SpecialButton_android_background, getDefaultBackground())
            printLog("initBackground[container : $container, background : $background]")
            setBackground(background)
        } catch(e: Exception) {
            e.printStackTrace()
            printLog("Exception caught on initBackground : ${e.message}")
        }
    }

    final override fun setBackground(background: Drawable?) {
        this.container?.background = (background ?: ResourcesCompat.getDrawable(resources, getDefaultBackground(), null))
    }

    protected open fun getDefaultBackground(): Int {
        return R.drawable.special_button
    }

    private fun initIcon(context: Context, typedArray: TypedArray) {
        try {
            icon = findViewById(R.id.icon)
            icon ?: return
            val iconDrawable: Drawable? = CpnUtils.getDrawable(context, typedArray, R.styleable.SpecialButton_android_src, getDefaultIcon())
            var tint: ColorStateList? = null
            if(typedArray.hasValue(R.styleable.SpecialButton_android_tint)) {
                tint = CpnUtils.getColorList(context, typedArray, R.styleable.SpecialButton_android_tint, Color.BLACK)
                iconDrawable?.setTintList(tint)
            }
            printLog("initIcon[icon : $iconDrawable, tint : $tint]")
            setIcon(iconDrawable)
        } catch(e: Exception) {
            e.printStackTrace()
            printLog("Exception caught on initIcon : ${e.message}")
        }
    }

    fun setIcon(icon: Drawable?) {
        this.icon?.setImageDrawable(icon ?: ResourcesCompat.getDrawable(resources, getDefaultIcon(), null))
    }

    protected open fun getDefaultIcon(): Int {
        return R.drawable.default_icon_button
    }

    protected open fun initFont(context: Context, typedArray: TypedArray) {
        try {
            text = findViewById(R.id.text)
            text ?: return

            Font.Builder(text)
                .setFamily(R.styleable.SpecialButton_android_fontFamily, R.font.poppins_bold)
                .setFontStyle(R.styleable.SpecialButton_android_textStyle, Typeface.NORMAL)
                .setFontSize(R.styleable.SpecialButton_android_textSize, R.dimen._12ssp)
                .setFontColor(R.styleable.SpecialButton_android_textColor, Color.parseColor("#FFFFFFFF"))
                .setAllCaps(R.styleable.SpecialButton_android_textAllCaps, false)
                .setIsUnderline(R.styleable.SpecialButton_textUnderline, false)
                .setText(R.styleable.SpecialButton_android_text, "Button")
                .build(context, typedArray)

            value = findViewById(R.id.value)
            value ?: return

            Font.Builder(value)
                .setFamily(R.styleable.SpecialButton_valueFontFamily, R.font.poppins_bold)
                .setFontStyle(R.styleable.SpecialButton_valueStyle, Typeface.NORMAL)
                .setFontSize(R.styleable.SpecialButton_valueSize, R.dimen._12ssp)
                .setFontColor(R.styleable.SpecialButton_valueColor, Color.parseColor("#FFFFFFFF"))
                .setAllCaps(R.styleable.SpecialButton_valueAllCaps, false)
                .setIsUnderline(R.styleable.SpecialButton_valueUnderline, false)
                .setText(R.styleable.SpecialButton_value, "")
                .build(context, typedArray)

        } catch(e: Exception) {
            e.printStackTrace()
            printLog("Exception caught on initText : ${e.message}")
        }
    }

}
