package com.telkomsel.mytelkomsel.component.button

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.util.AttributeSet

import com.telkomsel.mytelkomsel.component.Font
import com.telkomsel.mytelkomsel.component.R

open class SecondaryButton : PrimaryButton {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun getDefaultBackground(): Int {
        return R.drawable.secondary_button
    }

    override fun initFont(builder: Font.Builder, context: Context, typedArray: TypedArray) {
        super.initFont(builder, context, typedArray)
        builder.setFontColor(R.color.secondary_button_text)
    }

}
