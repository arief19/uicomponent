package com.telkomsel.mytelkomsel.component.button

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Paint
import android.util.AttributeSet

import com.telkomsel.mytelkomsel.component.Font

import com.telkomsel.mytelkomsel.component.R

class TextButton : SecondaryButton {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun getDefaultBackground(): Int {
        return R.drawable.text_button
    }

}
