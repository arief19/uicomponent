package com.telkomsel.mytelkomsel.component.button

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet

import androidx.appcompat.widget.AppCompatImageButton

import com.telkomsel.mytelkomsel.component.CpnUtils
import com.telkomsel.mytelkomsel.component.R

open class IconButton : AppCompatImageButton {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.IconButton)

        initBackground(context, typedArray)
        initIcon(context, typedArray)

        typedArray.recycle()
    }

    protected open fun initBackground(context: Context, typedArray: TypedArray) {
        try {
            val background = CpnUtils.getColorOrDrawable(context, typedArray, R.styleable.IconButton_android_background, getDefaultBackground())
            printLog("initBackground[background : $background]")
            setBackground(background)
        } catch(exc: Exception) {
            exc.printStackTrace()
            printLog("Exception caught on initBackground : " + exc.message)
        }
    }

    protected open fun initIcon(context: Context, typedArray: TypedArray) {
        try {
            val icon = CpnUtils.getDrawable(context, typedArray, R.styleable.IconButton_android_src, getDefaultIcon())
            printLog("initIcon[icon : $icon]")
            setImageDrawable(icon)
        } catch(exc: Exception) {
            exc.printStackTrace()
            printLog("Exception caught on initIcon : " + exc.message)
        }
    }

    protected open fun getDefaultBackground(): Int {
        return R.drawable.icon_button
    }

    private fun printLog(log: String) { }

    protected open fun getDefaultIcon(): Int {
        return R.drawable.default_icon_button
    }

}
