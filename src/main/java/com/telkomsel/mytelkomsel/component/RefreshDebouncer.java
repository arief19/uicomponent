package com.telkomsel.mytelkomsel.component;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class RefreshDebouncer extends AbstractDebouncer<RefreshDebouncer> implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout layout = null;

    public static RefreshDebouncer create(SwipeRefreshLayout layout) {
        return new RefreshDebouncer(layout)
            .setDelay(5000)
            .setType(AbstractDebouncer.DebounceType.EXACT);
    }

    private RefreshDebouncer(SwipeRefreshLayout layout) {
        this.layout = layout;
    }

    @Override
    public void onRefresh() {
        layout.setRefreshing(false);
        execute();
    }

    @Override
    protected void initialize() {
        this.layout.setOnRefreshListener(this);
    }
}
