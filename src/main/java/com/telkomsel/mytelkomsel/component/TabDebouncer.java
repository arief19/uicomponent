package com.telkomsel.mytelkomsel.component;

import com.google.android.material.tabs.TabLayout;

public class TabDebouncer extends AbstractDebouncer<TabDebouncer> implements TabLayout.OnTabSelectedListener {

    private TabLayout layout = null;

    public static TabDebouncer create(TabLayout layout) {
        return new TabDebouncer(layout)
            .setDelay(1000)
            .setType(DebounceType.RESET);
    }

    private TabDebouncer(TabLayout layout) {
        this.layout = layout;
    }

    @Override
    protected void initialize() {
        this.layout.addOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        execute();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) { }

    @Override
    public void onTabReselected(TabLayout.Tab tab) { }
}
