package com.telkomsel.mytelkomsel.component;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class UIState {
    public enum State { NONE, LOADING, EMPTY, ERROR, SUCCESS }

    public static UIState Builder() {
        return new UIState();
    }

    private State state;
    private List<ViewState> viewStateList;
    private Listener listener;

    private UIState() {
        state = State.NONE;
        viewStateList = new ArrayList<>();
    }

    public UIState build() {
        return this;
    }

    public State getState() { return state; }
    public void setState(State state) {
        if(this.state == state) return;
        if(listener != null) {
            listener.onBeforeStateChanged();
        }

        State prevState = this.state;
        this.state = state;
        printLog("setState : " + state);

        //hide everything first
        for(ViewState vs : viewStateList) {
            State s = vs.getState();
            if(s == state) continue;
            View view = vs.getView();
            if(view == null) continue;
            view.setVisibility(View.GONE);
            printLog(view + ".hide");
        }

        //show expected view
        for(ViewState vs : viewStateList) {
            State s = vs.getState();
            if(s != state) continue;
            View view = vs.getView();
            if(view == null) continue;
            view.setVisibility(View.VISIBLE);
            printLog(view + ".show");
        }

        if(listener != null) {
            listener.onStateChanged(prevState, this.state);
        }
    }

    public UIState setView(State state, View view) {
        try {
            for (ViewState vs : viewStateList) {
                State s = vs.getState();
                View v = vs.getView();
                if (s.equals(state) && v.equals(view)) {
                    throw new Exception("this view[" + v + "] already defined for '" + s + "' state");
                }
            }
            ViewState viewState = new ViewState(state, view);
            viewStateList.add(viewState);
        } catch(Exception exc) {
            printLog("Exception caught on setView : " + exc);
        }
        return this;
    }

    public List<View> getViews(State state) {
        List<View> ret = new ArrayList<>();
        for (ViewState vs : viewStateList) {
            if(!vs.getState().equals(state)) continue;;
            ret.add(vs.getView());
        }

        return ret;
    }

    public List<View> getViews() {
        List<View> ret = new ArrayList<>();
        for(ViewState vs : viewStateList) {
            ret.add(vs.getView());
        }

        return ret;
    }

    public List<ViewState> getViewStateList(State state) {
        List<ViewState> ret = new ArrayList<>();
        for (ViewState vs : viewStateList) {
            if(!vs.getState().equals(state)) continue;;
            ret.add(vs);
        }

        return ret;
    }

    public void clear(State state) {
        List<ViewState> cleared = getViewStateList(state);
        for(ViewState vs : cleared) {
            viewStateList.remove(vs);
        }
    }

    public void clearAll() {
        viewStateList.clear();
    }

    public UIState setListener(Listener listener) {
        this.listener = listener;
        return this;
    }

    private void printLog(String log) {
        System.out.println("#UIState#." + log);
    }

    public interface Listener {
        void onBeforeStateChanged();
        void onStateChanged(State prevState, State curState);
    }

    private static class ViewState {

        private UIState.State state = State.NONE;
        private View view = null;

        public ViewState(State state, View view) {
            this.state = state;
            this.view = view;
        }

        public State getState() { return state; }
        public View getView() { return view; }
    }
}
