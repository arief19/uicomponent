package com.telkomsel.mytelkomsel.component.util

import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import com.telkomsel.mytelkomsel.core.modules.BuildConfig

import com.telkomsel.mytelkomsel.core.modules.ModuleUtils
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class FileManager private constructor(builder: Builder) {

    companion object {
        private fun printLog(log: String) { }
    }

    private val context: Context = builder.context
    private val resolver: ContentResolver = context.contentResolver
    private val type: String? = builder.type
    private val volumeType: String? = builder.volumeType

    var dir: String? = builder.dir
    var fileName: String? = builder.fileName
    private var wildcards: String = ""
    var content: ByteArray? = builder.content
        set(content: ByteArray?) {
            val bytes = content ?: null
            field = if(maxSize > 0 && (bytes?.size ?: 0) >= maxSize) {
                null
            } else {
                bytes
            }

            printLog("setContent[maxSize : $maxSize, contentSize : ${field?.size}] : $field")
        }

    var maxSize: Long = builder.maxSize
    var mimeType: String? = builder.mimeType

    init {
        setUseWildcards(builder.useWildcards)
    }

    fun getContentAsString(): String? {
        content ?: return null
        return String(content!!)
    }

    fun isWildcards(): Boolean {
        return wildcards.isEmpty()
    }

    fun setUseWildcards(useWildcard: Boolean) {
        wildcards = if(useWildcard) "%" else ""
        printLog("setUseWildcards[$useWildcard] : $wildcards")
    }

    private fun getContentUri(): Uri {
        if(type == null || volumeType == null || Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) return MediaStore.Files.getContentUri(volumeType)
        return when(type) {
            Environment.DIRECTORY_DOWNLOADS -> {
                MediaStore.Downloads.getContentUri(volumeType)
            }
            else -> {
                MediaStore.Files.getContentUri(volumeType)
            }
        }
    }

    fun getFile(): File? {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return null
        }

        return File(File(Environment.getExternalStoragePublicDirectory(type), dir), fileName!!)
    }

    /**
     * get file uri from specified RELATIVE_DIR
     */
    fun getFileUri(): Uri? {
        printLog("getFileUri[$dir/$fileName]")
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return getFileUri(getContentUri())
        }

        val file = getFile()
        file ?: return null
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return FileProvider.getUriForFile(context, context.packageName + ".provider", file)
        }
        return Uri.fromFile(file)
    }

    /**
     * query file uri from ROOT uri
     */
    private fun getFileUri(uri: Uri?): Uri? {
        printLog("getFileUri[uri : $uri, $fileName]")
        uri ?: return null
        fileName ?: return null
        if(dir == null) dir = ""
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) return null
        try {
            val cursor = getQuery(
                " AND " + MediaStore.Files.FileColumns.DISPLAY_NAME + " = ?",
                arrayOf(fileName!!))

            cursor ?: return null
            var fileUri: Uri? = null
            if(cursor.moveToNext()) {
                val id = cursor.getLong(cursor.getColumnIndex(MediaStore.Files.FileColumns._ID))
                fileUri = Uri.withAppendedPath(uri, "" + id)
            }
            cursor.close()
            printLog("fileUri : $fileUri")
            return fileUri
        } catch(exc: Exception) {
            exc.printStackTrace()
            printLog("Exception caught on getFileUri : " + exc.message)
        }

        return null
    }

    private fun getQuery(): Cursor? {
        return getQuery(null, null)
    }

    private fun getQuery(selection: String?, selectionArgs: Array<String>?): Cursor? {
        if(dir == null) dir = ""
        val relativePath = File(type, dir!!).toString() + File.separator + wildcards
        val uri = getContentUri()
        val versionDoesntMatch = Build.VERSION.SDK_INT < Build.VERSION_CODES.Q
        printLog("getQuery.uri : $uri, relativePath : $relativePath, versionMatch : ${!versionDoesntMatch}")
        if(versionDoesntMatch) return null

        var sel = MediaStore.MediaColumns.RELATIVE_PATH + " like ?"
        selection?.apply {
            sel += selection
        }

        val selArgs: ArrayList<String> = ArrayList()
        selArgs.add(relativePath)
        selectionArgs?.apply {
            selArgs.addAll(this)
        }
        return resolver.query(
                uri,
                arrayOf(
                        MediaStore.MediaColumns._ID
                        , MediaStore.MediaColumns.DISPLAY_NAME
                        , MediaStore.MediaColumns.SIZE
                        , MediaStore.MediaColumns.RELATIVE_PATH
                )
                , sel
                , selArgs.toTypedArray()
                , null
        )
    }

    fun ls(): @JvmSuppressWildcards List<Uri> {
        printLog("ls")
        val uriList = ArrayList<Uri>()
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val uri = getContentUri()
            printLog("uri : $uri")
            try {
                val cursor = getQuery()
                cursor ?: return uriList
                while (cursor.moveToNext()) {
                    val id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID))
                    val name = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME))
                    val size = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns.SIZE))
                    var path = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.RELATIVE_PATH))
                    var log = "File[$id, $dir].name : $name, size : $size, path : $path"
                    printLog(log)
                    if(id <= 0) continue
                    uriList.add(Uri.withAppendedPath(uri, "" + id))
                }

                cursor.close()
            } catch(exc: Exception) {
                exc.printStackTrace()
                printLog("Exception caught on ls : " + exc.message)
            }
        }

        return uriList
    }

    fun isExists(): Boolean {
        printLog("isExists[$dir/$fileName]")
        fileName ?: return false
        if(dir == null) dir = ""

        var ret = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val uri = getContentUri()
            ret = isExists(uri)
            printLog("android >= Q[$ret] isExists : $uri")
        } else {
            val file = getFile()
            ret = file?.exists() == true
            printLog("android < Q isExists[$ret] : " + file?.absolutePath)
        }

        printLog("isExists[$dir/$fileName].ret : $ret")
        return ret
    }

    private fun isExists(uri: Uri): Boolean {
        printLog("isExists[uri : $uri, $fileName]")
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) return false
        return getFileUri(uri) != null
    }

    fun size(): Long {
        printLog("size[$dir/$fileName]")
        fileName ?: return 0
        if(dir == null) dir = ""

        var size: Long = 0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val cursor = getQuery()
            cursor ?: return 0
            if(cursor.moveToNext()) {
                size = cursor.getLong(cursor.getColumnIndex(MediaStore.MediaColumns.SIZE))
            } else {
                size = 0
            }
            cursor.close()
            printLog("android >= Q size : $size")
        } else {
            val file = getFile()
            size = file?.length() ?: 0
            printLog("android < Q size : $size")
        }

        printLog("size.ret : $size")
        return size
    }

    fun readFile() {
        printLog("readFile[$dir/$fileName]")
        fileName ?: return
        dir ?: return

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val uri = getContentUri()
            printLog("android >= Q readFile : $uri")
            readFile(uri)
        } else {
            val file = getFile()
            printLog("android < Q readFile.exists[" + file?.exists() + "] : " + file?.absolutePath)
            val fileContent = ModuleUtils.readFile(file)
            content = fileContent?.toByteArray()
            printLog("content : $content")
        }
    }

    /**
     * used by android Q or later only
     */
    private fun readFile(uri: Uri) {
        printLog("readFile[uri : $uri, $fileName]")
        content = null
        val fileUri = getFileUri(uri)
        fileUri ?: return
        try {
            val fid = resolver.openFileDescriptor(fileUri, "r")
            val fis = FileInputStream(fid?.fileDescriptor)
            val fileContent = ModuleUtils.readFile(fis)
            content = fileContent?.toByteArray()
            fid?.close()
            printLog("readFile.content : $content")
        } catch(exc: Exception) {
            exc.printStackTrace()
            printLog("Exception caught on readFile : " + exc.message)
        }
    }

    fun writeFile() {
        printLog("writeFile[$dir/$fileName] : " + content?.size)

        fileName ?: return
        dir ?: return
        content = content ?: "".toByteArray()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            delete()
            val path = File(type, dir).path
            printLog("android >= Q writeFile : $path/$fileName")

            val values = ContentValues()
            values.put(MediaStore.Files.FileColumns.DISPLAY_NAME, fileName)
            values.put(MediaStore.Files.FileColumns.RELATIVE_PATH, path)

            writeFile(values)
        } else {
            val file = getFile()
            printLog("android < Q writeFile : " + file?.absolutePath)
            ModuleUtils.writeFile(file, content!!)
            printLog("content : $content")
        }
    }

    /**
     * used by android Q or later only
     */
    private fun writeFile(values: ContentValues) {
        printLog("writeFile[$dir/$fileName] : $values")
        content ?: return
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) return
        try {
            val fileUri = resolver.insert(getContentUri(), values)
            printLog("uri : $fileUri")
            fileUri ?: return
            val fid = resolver.openFileDescriptor(fileUri, "w")
            val fos = FileOutputStream(fid?.fileDescriptor)
            ModuleUtils.writeFile(fos, content)
            fid?.close()
            printLog("writeFile success : $content")
        } catch (exc: Exception) {
            exc.printStackTrace()
            printLog("Exception caught on writeFile : " + exc.message)
        }
    }

    fun delete(): Int {
        printLog("delete[$dir/$fileName]")
        val fileUri = getFileUri()
        fileUri ?: return -1
        try {
            val deleted = resolver.delete(fileUri, null, null)
            printLog("deleted : $deleted")
            return deleted
        } catch(exc: Exception) {
            exc.printStackTrace()
            printLog("Exception caught on delete : " + exc.message)
        }

        return -1
    }

    fun createIntent(): Intent? {
        val uri = getFileUri()
        printLog("createIntent[$mimeType] : $uri")
        uri ?: return null
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.flags = intent.flags or Intent.FLAG_GRANT_READ_URI_PERMISSION
            intent.setDataAndType(uri, mimeType)
            return intent
        } catch(exc: Exception) {
            exc.printStackTrace()
            printLog("Exception caught on open : " + exc.message)
        }

        return null
    }

    class Builder(val context: Context) {

        var type: String? = Environment.DIRECTORY_DOWNLOADS
            private set
        var volumeType: String? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            MediaStore.VOLUME_EXTERNAL
        } else {
            null
        }
            private set
        var dir: String? = ""
            private set
        var fileName: String? = null
            private set
        var useWildcards: Boolean = false
            private set
        var content: ByteArray? = null
            private set
        var maxSize: Long = 0
            private set
        var mimeType: String? = "text/plain"
            private set

        fun setType(type: String?) = apply { this.type = type }
        fun setVolumeType(volumeType: String?) = apply { this.volumeType = volumeType }
        fun setDir(dir: String?) = apply { this.dir = dir }
        fun setFileName(fileName: String?) = apply { this.fileName = fileName }
        fun setUseWildcards(useWildcards: Boolean) = apply { this.useWildcards = useWildcards }
        fun setContent(content: ByteArray?) = apply { this.content = content }
        fun setMaxSize(maxSize: Long) = apply { this.maxSize = maxSize }
        fun setMimeType(mimeType: String?) = apply { this.mimeType = mimeType }
        fun build() = FileManager(this)
    }

}
