package com.telkomsel.mytelkomsel.component

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

class TestPaint : View {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) { init(context, attrs) }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) { init(context, attrs) }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) { init(context, attrs) }

    var contentWidth: Int = 0
        private set
    var contentHeight: Int = 300
        private set

    private val paint: Paint = Paint()
    private var bitmap: Bitmap? = null

    private fun init(context: Context?, attrs: AttributeSet?) {
        initPaint()
        initBitmap()
    }

    private fun initPaint() {
        setLayerType(LAYER_TYPE_SOFTWARE, null)
        setLayerType(LAYER_TYPE_HARDWARE, null)

        paint.style = Paint.Style.FILL_AND_STROKE
        paint.strokeWidth = resources.getDimensionPixelSize(R.dimen._3sdp).toFloat()
    }

    private fun initBitmap() {
        val drawable = resources.getDrawable(R.drawable.ic_loader, null)
        val bitmapWidth = drawable.intrinsicWidth
        val bitmapHeight = drawable.intrinsicHeight
        bitmap = Bitmap.createBitmap(bitmapWidth + 100, bitmapHeight + 100, Bitmap.Config.ARGB_8888)
        printLog("initBitmap[w : $bitmapWidth, h : $bitmapHeight] : $bitmap")
        bitmap ?: return
        val canvas = Canvas(bitmap!!)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        printLog("init bitmap canvas[w : " + canvas.width + ", h : " + canvas.height + "]")
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var w = suggestedMinimumWidth + paddingLeft + paddingRight + contentWidth
        w = resolveSize(w, widthMeasureSpec)

        var h = suggestedMinimumHeight + paddingTop + paddingBottom + contentHeight
        h = resolveSize(h, heightMeasureSpec)

        printLog("w : $w, h : $h")
        contentWidth = w
        contentHeight = h
        setMeasuredDimension(w, h)
    }

    override fun onDraw(canvas: Canvas?) {
        canvas ?: return
        drawContent(canvas)
    }

    private fun drawContent(canvas: Canvas) {
        val bounds: RectF = RectF()
        val margin = paint.strokeWidth / 2
        bounds.left = margin
        bounds.top = margin
        bounds.right = contentWidth.toFloat() - margin
        bounds.bottom = contentHeight.toFloat() - margin

        val path = Path()
        printLog("bounds : $bounds")
        canvas.drawArc(bounds, 0f, 290f, true, paint)

        bitmap?.let {
            canvas.drawBitmap(it, 0f, 0f, paint)
        }
    }

    private fun printLog(log: String) {
        println("DEBUG -> TestPaint.$log")
    }

}
