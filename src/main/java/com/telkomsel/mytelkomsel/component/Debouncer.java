package com.telkomsel.mytelkomsel.component;

public class Debouncer extends AbstractDebouncer<Debouncer> {

    public static Debouncer create() {
        return new Debouncer()
            .setDelay(1000)
            .setType(DebounceType.RESET);
    }

    private Debouncer() { }

    @Override
    protected void initialize() { }

    @Override
    public void execute() {
        super.execute();
    }
}
