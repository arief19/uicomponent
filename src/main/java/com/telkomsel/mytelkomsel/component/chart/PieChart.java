package com.telkomsel.mytelkomsel.component.chart;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.telkomsel.mytelkomsel.component.CpnUtils;
import com.telkomsel.mytelkomsel.component.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PieChart extends View {

    public static final int ACTUAL_SIZE = 360;

    public enum AnimType { NONE, SEQUENCE, PARALLEL }

    private int radius = 0;
    private Paint paint;

    //parameters
    private AnimType animType = AnimType.SEQUENCE;
    private int animDuration = 0;
    private float animValue = 0;
    private ValueAnimator animator = null;
    private int strokeWidth = 0;
    private int backStrokeColor = 0;
    private int valueStrokeColor = 0;
    private float minValue = 0, maxValue = 0;

    private Value background = null;
    private List<Value> values = new ArrayList<>();

    public PieChart(Context context) { super(context); }
    public PieChart(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PieChart(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public PieChart(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PieChart);

        initRadius();
        initAnimType(typedArray);
        initAnimator(typedArray);
        initSize(typedArray);
        initBackStrokeColor(typedArray);
        initValueStrokeColor(typedArray);
        initPaint();

        typedArray.recycle();
    }

    private void initRadius() {
        int defVal = getDefaultRadius();
        radius = getResources().getDimensionPixelSize(defVal);
        printLog("init[radius : " + getResources().getDimension(defVal) + ", in pixel : " + radius + "]");
    }

    protected int getDefaultRadius() {
        return R.dimen._26sdp;
    }

    private void initAnimType(TypedArray typedArray) {
        int animTypeIndex = typedArray.getInt(R.styleable.PieChart_animType, -1);
        animType = getDefaultAnimType();
        AnimType[] animTypes = AnimType.values();
        if(animTypeIndex != -1 && animTypeIndex < animTypes.length) {
            animType = animTypes[animTypeIndex];
        }

        printLog("initAnimType : " + animType);
    }

    protected AnimType getDefaultAnimType() {
        return AnimType.SEQUENCE;
    }

    private void initSize(TypedArray typedArray) {
        //stroke width
        int defVal = getResources().getDimensionPixelSize(getDefaultStrokeWidth());
        strokeWidth = typedArray.getDimensionPixelSize(R.styleable.PieChart_strokeWidth, defVal);

        //minValue
        minValue = typedArray.getInt(R.styleable.PieChart_minValue, getDefaultMinValue());

        //maxValue
        maxValue = typedArray.getInt(R.styleable.PieChart_maxValue, getDefaultMaxValue());

        printLog("initSize[strokeWidth : " + strokeWidth + ", minValue : " + minValue + ", maxValue : " + maxValue + "]");
    }

    protected int getDefaultStrokeWidth() {
        return R.dimen._5sdp;
    }

    protected int getDefaultMinValue() {
        return 0;
    }

    protected int getDefaultMaxValue() {
        return 100;
    }

    private void initBackStrokeColor(TypedArray typedArray) {
        backStrokeColor = typedArray.getColor(R.styleable.PieChart_backStrokeColor, getDefaultBackStrokeColor());
    }

    protected int getDefaultBackStrokeColor() {
        return 0xFFEDECF0;
    }

    private void initValueStrokeColor(TypedArray typedArray) {
        valueStrokeColor = typedArray.getColor(R.styleable.PieChart_valueStrokeColor, getDefaultValueStrokeColor());
    }

    protected int getDefaultValueStrokeColor() {
        return 0xFF0050AE;
    }

    private void initPaint() {
        setLayerType(LAYER_TYPE_SOFTWARE, null);
        setLayerType(LAYER_TYPE_HARDWARE, null);

        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
    }

    private void initAnimator(TypedArray typedArray) {
        animDuration = typedArray.getInt(R.styleable.PieChart_animDuration, getDefaultAnimDuration());

        animator = ValueAnimator.ofInt(0, 360);
        animator.setDuration(animDuration);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(animation -> {
            try {
                animValue = Float.valueOf("" + animation.getAnimatedValue());
                printLog("onAnimationUpdate : " + animValue);
                invalidate();
            } catch(Exception exc) {
                exc.printStackTrace();
                printLog("Exception caught onAnimationUpdate : " + exc.getMessage());
            }
        });
    }

    protected int getDefaultAnimDuration() {
        return 1000;
    }

    public void startChartAnimation() {
        if(animator == null || animType == AnimType.NONE) return;
        animator.start();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        printLog("onMeasure[widthMeasureSpec : " + widthMeasureSpec + ", heightMeasureSpec : " + heightMeasureSpec + "]");

        int chartW = getRadius();
        int wSpec = MeasureSpec.getSize(widthMeasureSpec);
        int desiredW = getSuggestedMinimumWidth() + getPaddingLeft() + getPaddingRight() + chartW;
        int w = resolveSize(desiredW, widthMeasureSpec);

        int chartH = getRadius();
        int hSpec = MeasureSpec.getSize(heightMeasureSpec);
        int desiredH = getSuggestedMinimumHeight() + getPaddingTop() + getPaddingBottom() + chartH;
        int h = resolveSize(desiredH, heightMeasureSpec);

//        printLog("padding : [" + getPaddingLeft() + ", " + getPaddingTop() + ", " + getPaddingRight() + ", " + getPaddingBottom() + "]");
//        printLog("chartSize : [" + chartW + ", " + chartH + "]");
//        printLog("wSpec : " + wSpec + ", desiredW : " + desiredW + ", w : " + w);
//        printLog("hSpec : " + hSpec + ", desiredH : " + desiredH + ", h : " + h);
        setMeasuredDimension(w, h);
        setRadius(Math.min(w, h));
    }

    private int getRadius() {
        return radius + (strokeWidth / 2);
    }

    private void setRadius(int radius) {
        int newRadius = radius - (strokeWidth / 2);
        if(newRadius == this.radius) return;
        printLog("setRadius[old : " + this.radius + ", new : " + newRadius + "]");
        this.radius = newRadius;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawPie(canvas);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startChartAnimation();
    }

    private Value validate(Value value) {
        if(value == null) return null;
        //set default color if none
        if(value.getColor() == 0) {
            value.setColor(getValueStrokeColor());
        }

        if(!value.validate(getMinValue(), getMaxValue(), getTotalValue(), 0, ACTUAL_SIZE)) {
            return null;
        }

        return value;
    }

    private void drawPie(Canvas canvas) {
        RectF bounds = new RectF();
        bounds.left = strokeWidth / 2;
        bounds.top = strokeWidth / 2;
        bounds.right = radius;
        bounds.bottom = radius;

        if(background == null) {
            background = new Value("background", getMinValue(), getMaxValue(), getBackStrokeColor());
            background.convert(getMinValue(), getMaxValue(), 0, ACTUAL_SIZE);
            values.add(0, background); //add background layer
        }

        printLog("drawPie : " + values.size());
        for(int i=0; i<values.size(); i++) {
            Value val = values.get(i);
            if(!val.isActive()) continue;
            float pos = (i == 0) ? ACTUAL_SIZE : animValue;//use this to animate
            drawValue(pos, ACTUAL_SIZE, val, bounds, paint, canvas);
        }
    }

    private void drawValue(float pos, float finalPos, Value value, RectF bounds, Paint paint, Canvas canvas) {
        int angleOffset = -90;
        paint.setColor(value.getColor());
        paint.setStrokeCap(value.getCap());
        paint.setPathEffect(value.getPathEffect());

        Path path = new Path();
        printLog("drawValue[" + value.getTag() + ", isInfinite : " + value.isInfinite() + "] : " + value.getStartValue() + ", angleOffset : " + angleOffset + ", value : " + value.getValue());
        float startVal = value.getFrom();
        float endVal = value.getTo();

        if(animType == AnimType.SEQUENCE) {
            if (startVal > pos) return; //don't draw this Value
            if (endVal > pos - startVal) {
                endVal = pos - startVal; //draw this Value until pos only
            }
        } else if(animType == AnimType.PARALLEL) {
            float percentage = Value.calcPercentage(pos, 0, finalPos);
            endVal = Value.calcValue(percentage, startVal, endVal) - startVal;
            printLog("percentage : " + percentage);
        }
        printLog("addArc[pos : " + pos + ", finalPos : " + finalPos + ", animType : " + animType + ", startValue : " + startVal + ", value : " + endVal + "]");
        path.addArc(bounds, startVal + angleOffset, endVal);
        canvas.drawPath(path, paint);
    }

    public AnimType getAnimType() { return animType; }
    public void setAnimType(AnimType animType) { this.animType = animType; }
    public int getStrokeWidth() { return strokeWidth; }
    public void setStrokeWidth(int strokeWidth) { this.strokeWidth = strokeWidth; }
    public int getBackStrokeColor() { return backStrokeColor; }
    public void setBackStrokeColor(int backStrokeColor) { this.backStrokeColor = backStrokeColor; }
    public int getValueStrokeColor() { return valueStrokeColor; }
    public void setValueStrokeColor(int valueStrokeColor) { this.valueStrokeColor = valueStrokeColor; }
    public float getMinValue() { return minValue; }
    public void setMinValue(float minValue) {
        if(minValue > maxValue) return;
        this.minValue = minValue;
    }
    public float getMaxValue() { return maxValue; }
    public void setMaxValue(float maxValue) {
        if(maxValue < minValue) return;
        this.maxValue = maxValue;
    }

    public void setRange(float minValue, float maxValue) {
        if(minValue > maxValue) return;
        setMinValue(minValue);
        setMaxValue(maxValue);
    }

    public float getTotalValue() {
        float total = 0;
        for(Value val : values) {
            if(val.isInfinite()) continue;
            total = Math.max(total, val.getValue());
        }
        return total;
    }

    public PieChart clearValues() {
        this.values.clear();
        background = null;
        return this;
    }

    public PieChart setValues(List<Value> values) {
        if(values == null) return this;
        clearValues();
        for(Value val : values) {
            addValue(val);
        }
        return this;
    }

    public PieChart addValue(Value value) {
        Value val = validate(value); //add only valid value
        printLog("addValue[value : " + value + "].validated : " + val);
        if(val == null) return this;
        values.add(val);
        Collections.sort(values, new Sorter());
        return this;
    }

    private void printLog(String log) {
        printLog("Class", log);
    }

    private static void printLog(String tag, String log) { }

    public static class Value {

        private String tag = "Value";

        //original value
        private float startValue = 0;
        private float value = 0;

        //calculated value
        private float from = 0;
        private float to = 0;

        private int color = 0xFFFDA22B;
        private boolean infinite = false;
        private Paint.Cap cap = Paint.Cap.ROUND;
        private PathEffect pathEffect = null;

        private boolean active = true;

        public Value(float value, int color) {
            this(0, value, color);
        }

        public Value(String tag, float value, int color) {
            this(tag, 0, value, color);
        }

        public Value(float startValue, float value, int color) {
            this("Value", startValue, value, color);
        }

        public Value(String tag, float startValue, float value, int color) {
            printLog("create Value[" + tag + ", " + startValue + ", " + value + ", " + color + "]");
            setTag(tag);
            setStartValue(startValue);
            setValue(value);
            setColor(color);
            setActive(value > startValue);
            printLog("created [isActive : " + isActive() + ", " + getStartValue() + ", " + getValue() + ", " + getColor() + "]");
        }

        public String getTag() { return tag; }
        public void setTag(String tag) { this.tag = tag; }
        public float getStartValue() { return startValue; }
        public void setStartValue(float startValue) { this.startValue = startValue; }
        public float getValue() { return value; }
        public void setValue(float value) { this.value = value; }
        public float getFrom() { return from; }
        public void setFrom(float from) { this.from = from; }
        public float getTo() { return to; }
        public void setTo(float to) { this.to = to; }
        public int getColor() { return color; }
        public void setColor(int color) { this.color = color; }
        public boolean isInfinite() { return infinite; }
        public void setInfinite(boolean infinite) { this.infinite = infinite; }
        public Paint.Cap getCap() { return cap; }
        public void setCap(Paint.Cap cap) { this.cap = cap; }
        public PathEffect getPathEffect() { return this.pathEffect; }
        public void setPathEffect(PathEffect pathEffect) { this.pathEffect = pathEffect; }
        public boolean isActive() { return active; }
        protected void setActive(boolean active) { this.active = active; }

        public float getRange() {
            return to - from;
        }

        public boolean validate(float minValue, float maxValue, float total, float targetMinValue, float targetMaxValue) {
            //make sure value is in range === minValue <= value <= maxValue
            float startVal = getStartValue();
            startVal = (startVal < minValue) ? minValue : (startVal > maxValue) ? maxValue : startVal;
            setStartValue(startVal);

            if (total > maxValue) return false; //exceeded
            float val = getValue();
            //adjust value based on total values
            val += total;
            val = (val < startVal) ? startVal : (val > maxValue) ? maxValue : val;
            setValue(val);

            convert(minValue, maxValue, targetMinValue, targetMaxValue);

            return true;
        }

        /**
         * convert startValue & value to be in range between min and max value of the target value
         * @param minValue
         * @param maxValue
         */
        public void convert(float minValue, float maxValue, float targetMinValue, float targetMaxValue) {
            printLog("convert[minValue : " + minValue + ", maxValue : " + maxValue + ", targetMinValue : " + targetMinValue + ", targetMaxValue : " + targetMaxValue + "]");
//            if(value < minValue || value > maxValue) return 0;
//            return (value - minValue) / (maxValue - minValue) * ACTUAL_SIZE;

            float fromPercentage = calcPercentage(startValue, minValue, maxValue);
            if (startValue < minValue) {
                from = minValue;
            } else if (startValue > maxValue) {
                from = maxValue;
            } else {
                from = calcValue(fromPercentage, targetMinValue, targetMaxValue);
            }

            float toPercentage = calcPercentage(value, minValue, maxValue);
            if(value < minValue) {
                to = minValue;
            } else if(value > maxValue) {
                to = maxValue;
            } else {
                to = calcValue(toPercentage, targetMinValue, targetMaxValue);
            }

            printLog("convert.result[startValue : " + startValue + ", fromPercentage : " + fromPercentage + ", from : " + from
                + ", value : " + value + ", toPercentage : " + toPercentage + ", to : " + to + "]");
        }

        @NonNull
        @Override
        public String toString() {
            return "Value." + tag + "[startValue : " + startValue + ", value : " + value
                    + ", from : " + from + ", to : " + to
                    + ", infinite : " + isInfinite() + "]";
        }

        /**
         * calculate percentage value (1 = 100%)
         * @param minValue
         * @param maxValue
         * @param value
         * @return
         */
        public static float calcPercentage(float value, float minValue, float maxValue) {
            return (value - minValue) / (maxValue - minValue);
        }

        public static float calcValue(float percentage, float minValue, float maxValue) {
            return (percentage * (maxValue - minValue)) + minValue;
        }

        private void printLog(String log) {
            PieChart.printLog(getTag(), log);
        }

    }

    public static class InfiniteValue extends Value {

        public InfiniteValue(int color) {
            super(0, color);
            setInfinite(true);
            setCap(Paint.Cap.BUTT);
            setPathEffect(new DashPathEffect(new float[]{5, 9}, 0));
        }

        @Override
        public boolean validate(float minValue, float maxValue, float total, float targetMinValue, float targetMaxValue) {
            setStartValue(minValue);
            setValue(maxValue);
            setActive(true);

            convert(minValue, maxValue, 0, ACTUAL_SIZE);
            return true;
        }

    }

    public static class Sorter implements Comparator<Value> {

        @Override
        public int compare(Value o1, Value o2) {
            if(o1.isInfinite() || o2.isInfinite()) return 1;
            return o1.getValue() >= o2.getValue() ? -1 : 1; //DESC
        }

    }

}
