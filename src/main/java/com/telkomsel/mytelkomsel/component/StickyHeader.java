package com.telkomsel.mytelkomsel.component;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Px;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StickyHeader {

    public static StickyHeader Builder(@NonNull Context context) {
        return new StickyHeader(context);
    }

    private Context context = null;
    private FrameLayout scrollView = null;
    private LinearLayout container = null;

    private List<StickyItem> items = null;
    private List<Attached> attachedList = null;
    private boolean created = false;

    private StickyHeader(Context context) {
        this.context = context;
        items = new ArrayList<>();
        attachedList = new ArrayList<>();
    }

    public FrameLayout getScrollView() { return scrollView; }
    public StickyHeader setScrollView(FrameLayout scrollView) {
        this.scrollView = scrollView;
        return this;
    }

    public LinearLayout getContainer() { return container; }
    public StickyHeader setContainer(LinearLayout container) {
        this.container = container;
        return this;
    }

    public StickyHeader addItem(View header, View lastItem) {
        StickyItem item = new StickyItem(header, lastItem);
        return addItem(item);
    }

    public StickyHeader addItem(StickyItem item) {
        //need to validate header before adding
        if(item.getHeader() == null) return this;
        items.add(item);
        return this;
    }

    public StickyHeader setItems(List<StickyItem> items) {
        reset();

        for(StickyItem item : items) {
            addItem(item);
        }

        return this;
    }

    public StickyHeader clearItems() {
        this.items.clear();
        attachedList.clear();
        return this;
    }

    public void destroy() {
        printLog("destroy");
        clearItems();

        if(scrollView != null) {
            scrollView.getViewTreeObserver().removeOnScrollChangedListener(this::onScrollChangeListener);
        }

        context = null;
        scrollView = null;
        container = null;
    }

	public void reset() {
		printLog("reset");
		clearItems();
		created = false;
	}

    /**
     * call build after all views is loaded to avoid unexpected result
     * @return
     */
    public void build() {
        printLog("build[created : " + created + "]");
        if(created) return;
        if(context == null) throw new NullPointerException("context may not null !!!");
        if(scrollView == null) throw new NullPointerException("scrollView may not null !!!");
        if(container == null) throw new NullPointerException("container may not null !!!");

        List<StickyItem> failedItems = new ArrayList<>();
        for (StickyItem item : items) {
            createAttached(item);
            int top = item.getAttached().getTop();
            int bottom = item.getAttached().getBottom();
            if(top < 0 || bottom < 0 || bottom <= top) {
                failedItems.add(item);
            }
        }

        printLog("failedItem : " + failedItems.size());
        for(StickyItem item : failedItems) {
            items.remove(item);
        }
        failedItems.clear();

        printLog("success item : " + items.size());
        if(items.size() <= 0) return;

        scrollView.getViewTreeObserver().addOnScrollChangedListener(this::onScrollChangeListener);
        created = true;
    }

    private void onScrollChangeListener() {
        onScrollChangeListener(scrollView, scrollView.getScrollX(), scrollView.getScrollY(), 0, 0);
    }

    private void onScrollChangeListener(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        validateAttached(scrollY);
        validatePosition(scrollY);
        removeDetached();
    }

    private void validatePosition(int scrollY) {
        for(StickyItem item : items) {
            if(isAttached(item)) continue;
            View header = item.getHeader();
            Attached attached = getAttached(item);
			header.setY(attached.getTop());
            printLog("scrollY : " + scrollY + ", Attached[" + attached.getPosition() + "] : " + attached.getTop() + "[" + header.getY() + "], " + attached.getBottom());
            printLog("validatePosition : " + attached);

            if(scrollY <= attached.getTop()) continue;
            int position = attached.getPosition();
            printLog("attach.view[" + position + ", " + container.getChildAt(position) + "][" + scrollY + " > " + attached.getTop() + "(y : " + header.getY() + ")]");
            attach(item);
        }
    }

    private void validateAttached(int scrollY) {
        for(Attached attached : attachedList) {
            StickyItem item = getItem(attached);
            View header = item.getHeader();
            printLog("scrollY : " + scrollY + ", Attached[" + attached.getPosition() + "] : " + attached.getTop() + "[" + header.getY() + "], " + attached.getBottom());
            if(scrollY > attached.getTop()) {
				int curY = scrollY;
				if(scrollY >= attached.getBottom()) {
					curY = attached.getBottom();
				}
				header.setY(curY);
                printLog("validateAttached : " + attached);
            } else {
                int position = attached.getPosition();
                printLog("detach.view[" + position + ", " + container.getChildAt(position) + "][" + scrollY + " > " + attached.getTop() + "(y : " + header.getY() + ")]");
                detach(item);
            }
        }
    }

    private StickyItem getItem(Attached attached) {
        for(StickyItem item : items) {
            if(!attached.equals(item.getAttached())) continue;
            return item;
        }

        return null;
    }

    private boolean isAttached(StickyItem item) {
        return attachedList.contains(item.getAttached());
    }

    private void removeDetached() {
        Attached removed = null;
        for(Attached attached : attachedList) {
            if(!attached.isDetached()) continue;
            removed = attached;
            break;
        }

        if(removed != null) {
            attachedList.remove(removed);
        }
    }

    //Attached Pool
    private Attached getAttached(StickyItem item) {
        Attached attached = item.getAttached();
        if(attached == null) {
            attached = createAttached(item);
        }
        return attached;
    }

    private Attached createAttached(StickyItem item) {
        printLog("createAttached : " + item);
        View header = item.getHeader();

        Attached attached = new Attached(header);
        item.setAttached(attached);
        int position = container.indexOfChild(header);
        attached.setPosition(position);
        calcStickyItem(header, item);

        return attached;
    }

    public boolean isCreated() { return created; }

    private void calcStickyItem(View source, StickyItem item) {
        Attached attached = item.getAttached();
        printLog("Before : " + attached);

        int top = calcTop(source);
        int bottom = calcBottom(source, item.getItem());
        attached.setTop(top);
        attached.setBottom(bottom);

        printLog("After : " + attached);
    }

    private void attach(StickyItem item) {
        try {
            Attached attached = getAttached(item);
            attached.setDetached(false);
            View header = item.getHeader();
			calcStickyItem(header, item);
            attachedList.add(attached);

            if (header.getBackground() == null) {
                header.setBackgroundColor(0xFFFFFFFF); //to make elevation working. the background may not null
            }
			
			header.setElevation(12);
        } catch(Exception exc) {
            exc.printStackTrace();
            printLog("Exception caught on attach : " + exc);
        }
    }

    private void detach(StickyItem item) {
        try {
            Attached attached = item.getAttached();
            attached.setDetached(true);
            View header = item.getHeader();
			header.setY(attached.getTop());
			
			header.setElevation(0);
        } catch(Exception exc) {
            exc.printStackTrace();
            printLog("Exception caught on detach : " + exc);
        }
    }

    private int calcTop(View view) {
        return Math.round(view.getY());
    }

    private int calcBottom(View source, View target) {
        //get child item
        if(target == null) {
            int idx = container.indexOfChild(source) + 1;
            if(idx < container.getChildCount()) {
                target = container.getChildAt(idx);
            }
        }

        if(target == null) return -1;
        int headerHeight = source.getMeasuredHeight();
        int itemY = Math.round(target.getY());
        int itemHeight = target.getMeasuredHeight();
        int itemBottom = (itemY + itemHeight) - headerHeight;
        return itemBottom;
    }

    private void printLog(String log) { }

    public static class StickyItem {
        private View header = null;
        private View item = null;
        private Attached attached = null;

        public StickyItem(View header, View item) {
            this.header = header;
            this.item = item;
        }

        public View getHeader() { return header; }
        public View getItem() { return item; }
        public void setItem(View item) { this.item = item; }
        public Attached getAttached() { return attached; }
        public void setAttached(Attached attached) { this.attached = attached; }
    }

    private static class Attached {
        private View header = null;
        private int position = -1;
        private int top = -1;
        private int bottom = -1;
        private boolean detached = true;

        public Attached(View header) {
            this.header = header;
            this.detached = false;
        }

        public View getheader() { return header; }
        public void setheader(View header) { this.header = header; }
        public int getPosition() { return position; }
        public void setPosition(int position) { this.position = position; }
        public int getTop() { return top; }
        public void setTop(int top) { this.top = top; }
        public int getBottom() { return bottom; }
        public void setBottom(int bottom) { this.bottom = bottom; }
        public boolean isDetached() { return detached; }
        public void setDetached(boolean detached) { this.detached = detached; }

        @NonNull
        @Override
        public String toString() {
            return "Attached[position : " + position + ", top : " + top + ", bottom : " + bottom + ", detached : " + detached + "], " +
                    "header[x : " + header.getX() + ", y : " + header.getY() + ", w : " + header.getLayoutParams().width + ", h : " + header.getLayoutParams().height + "]";
        }
    }
}
