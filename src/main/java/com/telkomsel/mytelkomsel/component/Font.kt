package com.telkomsel.mytelkomsel.component

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.util.TypedValue
import android.widget.TextView
import androidx.annotation.StyleableRes
import androidx.core.content.res.ResourcesCompat
import java.lang.Exception

class Font private constructor(builder: Builder) {

    val textView: TextView? = builder.textView

    class Builder constructor(val textView: TextView?) {

        @StyleableRes
        var familyId: Int? = null
            private set
        var family: Int = R.font.telkomsel_batik_sans_bold
            private set

        @StyleableRes
        var fontStyleId: Int? = null
            private set
        var fontStyle: Int = Typeface.BOLD
            private set

        @StyleableRes
        var fontSizeId: Int? = null
            private set
        var fontSize: Int = R.dimen._12ssp
            private set

        @StyleableRes
        var allCapsId: Int? = null
            private set
        var allCaps: Boolean = false
            private set

        @StyleableRes
        var fontColorId: Int? = null
            private set
        var fontColor: Int = Color.parseColor("#FF000000")
            private set

        @StyleableRes
        var textId: Int? = null
            private set
        var text: String? = ""
            private set

        @StyleableRes
        var isUnderlineId: Int? = null
            private set
        var isUnderline: Boolean = false
            private set

        fun setFamily(@StyleableRes id: Int, family: Int): Builder {
            this.familyId = id
            this.family = family
            return this
        }

        fun setFamily(family: Int): Builder {
            this.family = family
            return this
        }

        fun setFontStyle(@StyleableRes id: Int, fontStyle: Int): Builder {
            this.fontStyleId = id
            this.fontStyle = fontStyle
            return this
        }

        fun setFontStyle(fontStyle: Int): Builder {
            this.fontStyle = fontStyle
            return this
        }

        fun setFontSize(@StyleableRes id: Int, fontSize: Int): Builder {
            this.fontSizeId = id
            this.fontSize = fontSize
            return this
        }

        fun setFontSize(fontSize: Int): Builder {
            this.fontSize = fontSize
            return this
        }

        fun setAllCaps(@StyleableRes id: Int, allCaps: Boolean): Builder {
            this.allCapsId = id
            this.allCaps = allCaps
            return this
        }

        fun setAllCaps(allCaps: Boolean): Builder {
            this.allCaps = allCaps
            return this
        }

        fun setFontColor(@StyleableRes id: Int, fontColor: Int): Builder {
            printLog("setFontColor[$id, $fontColor]")
            this.fontColorId = id
            this.fontColor = fontColor
            return this
        }

        fun setFontColor(fontColor: Int): Builder {
            this.fontColor = fontColor
            return this
        }

        fun setText(@StyleableRes id: Int, text: String?): Builder {
            this.textId = id
            this.text = text ?: ""
            return this
        }

        fun setText(text: String?): Builder {
            this.text = text ?: ""
            return this
        }

        fun setIsUnderline(@StyleableRes id: Int, isUnderline: Boolean): Builder {
            this.isUnderlineId = id
            this.isUnderline = isUnderline
            return this
        }

        fun setIsUnderline(isUnderline: Boolean): Builder {
            this.isUnderline = isUnderline
            return this
        }

        private fun printLog(log: String) { }

        fun build(context: Context, typedArray: TypedArray): Font? {

            printLog("build[$textView, $context, $typedArray]")
            textView?.let {
                try {
                    buildTypeface(context, typedArray)
                    buildSize(context, typedArray)
                    buildColor(context, typedArray)
                    buildAllCaps(context, typedArray)
                    buildText(context, typedArray)
                    buildIsUnderline(context, typedArray)
                } catch(exc: Exception) {
                    exc.printStackTrace()
                    printLog("Exception caught on build : " + exc.message)
                }
            }

            return Font(this)
        }

        private fun buildTypeface(context: Context, typedArray: TypedArray) {
            printLog("buildTypeFace[$familyId, $family, $fontStyleId, $fontStyle]")
            familyId ?: return
            fontStyleId ?: return

            val resId = typedArray.getResourceId(familyId!!, family)
            if(resId <= 0) return
            val typeFace = ResourcesCompat.getFont(context, resId)

            val style = typedArray.getInt(fontStyleId!!, fontStyle)
            textView?.setTypeface(typeFace, style)
        }

        private fun buildSize(context: Context, typedArray: TypedArray) {
            printLog("buildSize[$fontSizeId]")
            fontSizeId ?: return
            val defSize = context.resources.getDimensionPixelSize(fontSize)
            val size = typedArray.getDimensionPixelSize(fontSizeId!!, defSize)
            textView?.setTextSize(TypedValue.COMPLEX_UNIT_PX, size.toFloat())
        }

        private fun buildColor(context: Context, typedArray: TypedArray) {
            printLog("buildColor[$fontColorId, $fontColor]")
            fontColorId ?: return
            val colors = CpnUtils.getColorList(context, typedArray, fontColorId!!, fontColor)
            printLog("colors : $colors")
            textView?.setTextColor(colors)
        }

        private fun buildAllCaps(context: Context, typedArray: TypedArray) {
            printLog("buildAllCaps[$allCapsId]")
            allCapsId ?: return
            val isAllCaps = typedArray.getBoolean(allCapsId!!, this.allCaps)
            textView?.isAllCaps = isAllCaps
        }

        private fun buildText(context: Context, typedArray: TypedArray) {
            printLog("buildText[$textId]")
            textId ?: return
            val text = typedArray.getString(textId!!)
            textView?.text = text ?: this.text ?: ""
        }

        private fun buildIsUnderline(context: Context, typedArray: TypedArray) {
            printLog("buildIsUnderline[$isUnderlineId]")
            isUnderlineId ?: return
            val isUnderline = typedArray.getBoolean(isUnderlineId!!, this.isUnderline)
            if(isUnderline) {
                textView?.apply {
                    paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
                }
            }
        }
    }
}
