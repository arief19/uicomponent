package com.telkomsel.mytelkomsel.component.dialog

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import com.telkomsel.mytelkomsel.component.CpnUtils
import com.telkomsel.mytelkomsel.component.R

open class ImageDialog protected constructor(builder: Builder?, context: Context) : GeneralDialog(builder, context) {

    private var builder: Builder? = null
    protected var ivClose: ImageView? = null
        private set
    protected var ivIcon: ImageView? = null
        private set

    init {
        this.builder = builder ?: Builder(context)
    }

    override fun onLayoutSet(container: View, inflater: LayoutInflater) {
        initIcon()
    }

    protected open fun initIcon() {
        builder ?: return
        ivIcon = findViewById(builder!!.iconId)
        printLog("initIcon[drawable : ${builder?.icon}, url : ${builder?.iconUrl}, ivIcon : $ivIcon]")

        builder?.let {
            //load icon from url
            if(!TextUtils.isEmpty(it.iconUrl)) {
                CpnUtils.changeImage(context, it.iconUrl, null) { retVal ->
                    setIcon(retVal ?: it.icon)
                }
            } else if(it.icon != null) {
                setIcon(it.icon)
            }
        }
    }

    private fun setIcon(icon: Drawable?) {
        printLog("setIcon : $icon")
        ivIcon?.apply {
            setImageDrawable(icon)
            visibility = if(icon != null) ViewGroup.VISIBLE else ViewGroup.GONE
        }
    }

    override fun initButton(view: View?) {
        super.initButton(view)

        builder ?: return

        printLog("initButton[$view]")
        view?.apply {
            ivClose = findViewById(builder!!.closeId)
            ivClose?.apply {
                setOnClickListener {
                    dismiss()
                }
                visibility = if(builder!!.showCloseButton) ViewGroup.VISIBLE else ViewGroup.GONE
            }
        }
    }

    open class Builder(context: Context) : GeneralDialog.Builder(context) {

        @IdRes
        var iconId: Int = R.id.ivIcon
            private set
        var icon: Drawable? = null
            private set
        var iconUrl: String? = null
            private set
        @IdRes
        var closeId: Int = R.id.ivClose
            private set
        var showCloseButton: Boolean = true
            private set

        init {
            setLayout(R.layout.image_dialog)
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }

        fun setIconId(@IdRes iconId: Int): Builder {
            this.iconId = iconId
            return this
        }

        fun setIcon(icon: Drawable?): Builder {
            this.icon = icon
            return this
        }

        fun setIconUrl(iconUrl: String?): Builder {
            printLog("setIconUrl : $iconUrl")
            this.iconUrl = iconUrl
            return this
        }

        fun setCloseId(@IdRes closeId: Int): Builder {
            this.closeId = closeId
            return this
        }

        fun setShowCloseButton(show: Boolean): Builder {
            this.showCloseButton = show
            return this
        }

        override fun build(): ImageDialog {
            return ImageDialog(this, context)
        }

    }

    companion object {

        private fun printLog(log: String) { }

    }

}
