package com.telkomsel.mytelkomsel.component.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.core.content.res.ResourcesCompat.ID_NULL
import com.telkomsel.mytelkomsel.component.R

open class GeneralDialog protected constructor(builder: Builder?, context: Context) : android.app.Dialog(context) {

    private var builder: Builder? = null
    protected var tvTitle: TextView? = null
        private set
    protected var tvDescription: TextView? = null
        private set
    protected var btnPrimary: Button? = null
        private set
    protected var btnSecondary: Button? = null
        private set

    init {
        this.builder = builder ?: Builder(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        builder ?: return
        initWindow()

        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(builder!!.layout, null)
        printLog("inflate : " + builder!!.layout)
        setContentView(view)

        onLayoutSet(view, inflater)
        createHeaderView(view, inflater)
        createContentView(view, inflater)
        createFooterView(view, inflater)

        initBackground(view)
        initButton(view)
        onCreateDialog()

        setOnDismissListener { this.onDismiss() }
    }

    /**
     * base layout has been set. time to create some custom layout based on some custom parameter... WHATEVER...
     */
    protected open fun onLayoutSet(container: View, inflater: LayoutInflater) { }

    private fun onDismiss() {
        builder ?: return
        printLog("onDismiss")
        builder!!.onDismiss?.onDialog(this)
    }

    private fun initBackground(container: View?) {
        builder ?: return
        printLog("initBackground[${builder?.background}]")
        val useBackground = builder!!.isFlagOn(USE_BACKGROUND)
        if(useBackground) {
            builder?.background ?: return
            container?.background = builder?.background
        } else {
            container?.background = ColorDrawable(Color.TRANSPARENT)
        }
    }

    protected open fun createHeaderView(container: View, inflater: LayoutInflater) {
        builder ?: return
        val header: ViewGroup? = container.findViewById(builder!!.headerLayoutId)
        val show = builder!!.isFlagOn(USE_HEADER)
        header?.apply {
            replaceViewIfExists(this, inflater, builder!!.headerLayout)
            visibility = if(show) ViewGroup.VISIBLE else ViewGroup.GONE
        }

        tvTitle = findViewById(builder!!.titleId)
        tvTitle?.apply {
            text = builder!!.title
            visibility = if (builder!!.isFlagOn(USE_TITLE)) ViewGroup.VISIBLE else ViewGroup.GONE
        }

        tvDescription = findViewById(builder!!.descriptionId)
        tvDescription?.apply {
            text = builder!!.description
            visibility = if (builder!!.isFlagOn(USE_DESC)) ViewGroup.VISIBLE else ViewGroup.GONE
        }

        printLog("createHeaderView[header : $header, tvTitle : $tvTitle, tvDescription : $tvDescription, visible : $show")
    }

    private fun onCreateDialog() {
        builder ?: return
        printLog("onCreateDialog[${builder?.cancelable}, ${builder?.canceledOnTouchOutside}]")
        builder?.let {
            setCancelable(it.cancelable)
            setCanceledOnTouchOutside(it.canceledOnTouchOutside)
            it.onCreate?.onDialog(this)
        }
    }

    protected fun replaceViewIfExists(container: ViewGroup?, inflater: LayoutInflater, @LayoutRes id: Int) {
        container?.apply {
            if(id != ID_NULL) {
                try {
                    val newView = inflater.inflate(id, this, false)
                    newView?.apply {
                        container.removeAllViews()
                        container.addView(newView)
                    }
                } catch (exc: Exception) {
                    exc.printStackTrace()
                }
            }
        }
    }

    protected open fun createContentView(container: View, inflater: LayoutInflater) {
        builder ?: return
        val content: ViewGroup? = container.findViewById(builder!!.contentLayoutId)
        val show = builder!!.isFlagOn(USE_CONTENT)
        content?.apply {
            replaceViewIfExists(this, inflater, builder!!.contentLayout)
            this.visibility = if(show) ViewGroup.VISIBLE else ViewGroup.GONE
        }

        printLog("createContentView[content : $content, visible : $show")
    }

    protected open fun createFooterView(container: View, inflater: LayoutInflater) {
        builder ?: return
        val footer: ViewGroup? = container.findViewById(builder!!.footerLayoutId)
        val show = builder!!.isFlagOn(USE_FOOTER)
        footer?.apply {
            replaceViewIfExists(this, inflater, builder!!.footerLayout)
            this.visibility = if(show) ViewGroup.VISIBLE else ViewGroup.GONE
        }

        printLog("createFooterView[footer : $footer, visible : $show")
    }

    private fun initWindow() {
        window?.apply {
            requestFeature(Window.FEATURE_NO_TITLE)
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            printLog("init background")
        }
    }

    protected open fun initButton(view: View?) {
        builder ?: return
        view?.apply {
            btnPrimary = findViewById(builder!!.primaryId)
            btnPrimary?.apply {
                text = builder!!.primaryText
                setOnClickListener { onClick(builder!!.onPrimaryButtonClicked) }
                visibility = if(builder!!.isFlagOn(USE_PRIMARY)) ViewGroup.VISIBLE else ViewGroup.GONE
            }

            btnSecondary = findViewById(builder!!.secondaryId)
            btnSecondary?.apply {
                text = builder!!.secondaryText
                setOnClickListener { onClick(builder!!.onSecondaryButtonClicked) }
                visibility = if(builder!!.isFlagOn(USE_SECONDARY)) ViewGroup.VISIBLE else ViewGroup.GONE
            }
        }

        printLog("initButton[btnPrimary : $btnPrimary, btnSecondary : $btnSecondary]")
    }

    protected fun onClick(listener: Listener?) {
        if(listener == null) {
            dismiss()
        } else {
            listener.onClick()
        }
    }

    override fun onStart() {
        super.onStart()

        setLayout(getDefaultWidth(), getDefaultHeight())
    }

    protected open fun getDefaultWidth(): Int {
        return (context.resources.displayMetrics.widthPixels * 0.85).toInt()
    }

    protected open fun getDefaultHeight(): Int {
        return ViewGroup.LayoutParams.WRAP_CONTENT
    }

    protected fun setLayout(width: Int, height: Int) {
        window?.apply {
            setLayout(width, height)
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setBackgroundDrawableResource(android.R.color.transparent)
        }
        printLog("setLayout[$width, $height]")
    }

    open class Builder(val context: Context) {

        @LayoutRes
        var layout: Int = R.layout.dialog
            private set
        @LayoutRes
        var headerLayout: Int = R.layout.dialog_header
            private set
        @IdRes
        var headerLayoutId: Int = R.id.dialog_header
            private set
        @LayoutRes
        var contentLayout: Int = R.layout.dialog_content
            private set
        @IdRes
        var contentLayoutId: Int = R.id.dialog_content
            private set
        @LayoutRes
        var footerLayout: Int = R.layout.dialog_footer
            private set
        @IdRes
        var footerLayoutId: Int = R.id.dialog_footer
            private set

        /**
         * use custom background if set and USE_BACKGROUND flag is ON
         * will use the existing background set by xml if this value is NULL
         * if USE_BACKGROUND flag is off then Color.TRANSPARENT value will be used instead
         */
        var background: Drawable? = null
            private set
        @IdRes
        var titleId: Int = R.id.tvTitle
            private set
        var title: String? = "Title"
            private set
        @IdRes
        var descriptionId: Int = R.id.tvDescription
            private set
        var description: String? = "Description"
            private set
        @IdRes
        var primaryId: Int = R.id.btnPrimary
            private set
        var primaryText: String? = "Ok"
            private set
        @IdRes
        var secondaryId: Int = R.id.btnSecondary
            private set
        var secondaryText: String? = "Cancel"
            private set
        var onPrimaryButtonClicked: Listener? = null
            private set
        var onSecondaryButtonClicked: Listener? = null
            private set
        var onCreate: OnDialogListener? = null
            private set
        var onDismiss: OnDialogListener? = null
            private set
        var cancelable: Boolean = true
            private set
        var canceledOnTouchOutside: Boolean = true
            private set
        var flags: Long = (USE_HEADER or
                USE_CONTENT or
                USE_FOOTER or
                USE_TITLE or
                USE_DESC or
                USE_PRIMARY or
                USE_SECONDARY or
                USE_BACKGROUND).toLong()
            private set

        open fun setLayout(@LayoutRes layout: Int): Builder {
            this.layout = layout
            //layout changed.. remove all default layout
            setHeaderLayout(ID_NULL)
            setContentLayout(ID_NULL)
            setFooterLayout(ID_NULL)
            return this
        }

        fun setHeaderLayout(@LayoutRes headerLayout: Int): Builder {
            this.headerLayout = headerLayout
            return this
        }

        fun setHeaderLayoutId(@IdRes headerLayoutId: Int): Builder {
            this.headerLayoutId = headerLayoutId
            return this
        }

        fun setContentLayout(@LayoutRes contentLayout: Int): Builder {
            this.contentLayout = contentLayout
            return this
        }

        fun setContentLayoutId(@IdRes contentLayoutId: Int): Builder {
            this.contentLayoutId = contentLayoutId
            return this
        }

        fun setFooterLayout(@LayoutRes footerLayout: Int): Builder {
            this.footerLayout = footerLayout
            return this
        }

        fun setFooterLayoutId(@IdRes footerLayoutId: Int): Builder {
            this.footerLayoutId = footerLayoutId
            return this
        }

        fun setBackground(background: Drawable?): Builder {
            this.background = background
            return this
        }

        fun setTitleId(@IdRes titleId: Int): Builder {
            this.titleId = titleId
            return this
        }

        fun setTitle(title: String?): Builder {
            this.title = title ?: this.title
            printLog("setTitle[$title] : ${this.title}")
            return this
        }

        fun setDescriptionId(@IdRes descriptionId: Int): Builder {
            this.descriptionId = descriptionId
            return this
        }

        fun setDescription(description: String?): Builder {
            this.description = description ?: this.description
            printLog("setDescription[$description] : ${this.description}")
            return this
        }

        fun setPrimaryId(@IdRes primaryId: Int): Builder {
            this.primaryId = primaryId
            return this
        }

        fun setPrimaryText(text: String?): Builder {
            this.primaryText = text ?: this.primaryText
            return this
        }

        fun setSecondaryId(@IdRes secondaryId: Int): Builder {
            this.secondaryId = secondaryId
            return this
        }

        fun setSecondaryText(text: String?): Builder {
            this.secondaryText = text ?: this.secondaryText
            return this
        }

        fun setOnCreateListener(listener: OnDialogListener?): Builder {
            this.onCreate = listener
            return this
        }

        fun setOnDismissListener(listener: OnDialogListener?): Builder {
            this.onDismiss = listener
            return this
        }

        fun setOnPrimaryButtonClicked(listener: Listener?): Builder {
            this.onPrimaryButtonClicked = listener
            return this
        }

        fun setOnSecondaryButtonClicked(listener: Listener?): Builder {
            this.onSecondaryButtonClicked = listener
            return this
        }

        fun setCancelable(cancelable: Boolean): Builder {
            this.cancelable = cancelable
            return this
        }

        fun setCanceledOnTouchOutside(canceledOnTouchOutside: Boolean): Builder {
            this.canceledOnTouchOutside = canceledOnTouchOutside
            return this
        }

        fun setFlag(flag: Long): Builder {
            val newFlags = flags or flag
            printLog("setFlag[flag : " + Integer.toBinaryString(flag.toInt()) +
                    ", prev : " + Integer.toBinaryString(flags.toInt()) +
                    ", cur : " + Integer.toBinaryString(newFlags.toInt()))
            flags = newFlags
            return this
        }

        fun unsetFlag(flag: Long): Builder {
            val newFlags = flags and flag.inv()
            printLog("unsetFlag[flag : " + Integer.toBinaryString(flag.inv().toInt()) +
                    ", prev : " + Integer.toBinaryString(flags.toInt()) +
                    ", cur : " + Integer.toBinaryString(newFlags.toInt()))
            flags = newFlags
            return this
        }

        fun isFlagOn(flag: Int): Boolean {
            return (flags and flag.toLong()) == flag.toLong()
        }

        open fun build() = GeneralDialog(this, context)

    }

    interface Listener {
        fun onClick()
    }

    interface OnDialogListener {
        fun onDialog(dialog: android.app.Dialog?)
    }

    companion object {

        const val USE_HEADER = 0x01
        const val USE_CONTENT = 0x02
        const val USE_FOOTER = 0x04
        const val USE_TITLE = 0x08
        const val USE_DESC = 0x10
        const val USE_PRIMARY = 0x20
        const val USE_SECONDARY = 0x40
        const val USE_BACKGROUND = 0x80

        private fun printLog(log: String) {
        }

    }

}
