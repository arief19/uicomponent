package com.telkomsel.mytelkomsel.component.dialog

import android.content.Context
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import com.telkomsel.mytelkomsel.component.R

open class LoaderDialog protected constructor(builder: Builder?, context: Context) : GeneralDialog(builder, context) {

    private var ivLoader: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ivLoader = findViewById(R.id.ivLoader)
        ivLoader?.apply {
            val rotateAnimation = RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
            rotateAnimation.apply {
                repeatCount = Animation.INFINITE
                duration = 1000
                interpolator = LinearInterpolator()
            }
            animation = rotateAnimation
        }
    }

    override fun onStart() {
        super.onStart()

        startAnimation()
    }

    override fun getDefaultWidth(): Int {
        return (context.resources.displayMetrics.widthPixels * 0.9).toInt()
    }

    private fun startAnimation() {
        if(!isShowing) return
        ivLoader?.apply {
            startAnimation(animation)
        }
    }

    class Builder(context: Context) : GeneralDialog.Builder(context) {

        init {
            setLayout(R.layout.dialog_loader)
            setTitle("Number Verification")
            setDescription("Please wait for the number being verified")
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }

        override fun build(): LoaderDialog {
            return LoaderDialog(this, context)
        }

    }

}