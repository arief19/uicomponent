package com.telkomsel.mytelkomsel.component;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleableRes;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.NestedScrollView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

public class CpnUtils {

    private CpnUtils() { } //protect class from being instantiated

    public static void changeImage(ImageView image, Drawable resource) {
        image.setImageDrawable(resource);
    }

    public static void changeImage(ImageView image, String url) {
        changeImage(image, url, null, null);
    }

    public static void changeImage(ImageView image, String url, Drawable defaultImage) {
        changeImage(image, url, defaultImage, null);
    }

    public static void changeImage(ImageView image, String url, @DrawableRes int id) {
        try {
            Drawable defaultImage = ContextCompat.getDrawable(image.getContext(), id);
            changeImage(image, url, defaultImage, null);
        } catch(Exception exc) {
            exc.printStackTrace();
        }
    }

    public static void changeImage(ImageView image, String url, OnFinishedListener<Drawable> listener) {
        changeImage(image, url, null, listener);
    }

    public static void changeImage(final ImageView image, String url, final Drawable defaultImage, final OnFinishedListener<Drawable> listener) {
        if(image == null) return;
        image.setVisibility(View.VISIBLE);
        changeImage(image.getContext(), url, defaultImage, new OnFinishedListener<Drawable>() {
            @Override
            public void onFinished(Drawable retVal) {
                if(retVal != null) {
                    changeImage(image, retVal);
                }
                if(listener != null) {
                    listener.onFinished(retVal);
                }
            }
        });
    }

    public static void changeImage(Context context, String url, Drawable defaultImage, OnFinishedListener<Drawable> listener) {
        if("".equals(url)) {
            if(defaultImage != null && listener != null) {
                listener.onFinished(defaultImage);
            }

            return;
        }

        Glide.with(context)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .error(defaultImage)
            .into(new CustomTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    if(resource == null) {
                        resource = defaultImage;
                    }

                    if(listener != null) {
                        listener.onFinished(resource);
                    }
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {

                }

                @Override
                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                    if(listener != null) {
                        listener.onFinished(errorDrawable);
                    }
                }
            });
    }

    public static void requestFocus(FrameLayout scrollView, View target) {
        requestFocus(scrollView, target, 3000);
    }

    public static void requestFocus(FrameLayout scrollView, View target, int speed) {
        requestFocus(scrollView, target, speed, 2000);
    }

    public static void requestFocus(FrameLayout scrollView, View target, int speed, int delay) {
        printLog("requestFocus : " + scrollView + ", " + target);

        scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                printLog("onContainerAttached : " + target.getY());
                scrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                scrollView.postDelayed(() -> {
                    int y = (int) target.getY();
                    if(scrollView instanceof ScrollView) {
                        ((ScrollView) scrollView).smoothScrollTo(0, y);
                    } else if(scrollView instanceof NestedScrollView) {
                        ((NestedScrollView) scrollView).smoothScrollTo(0, y, speed);
                    }
                }, delay);
            }
        });
    }

    private static void printLog(String log) { }

    public interface OnFinishedListener<T> {
        void onFinished(T retVal);
    }

    public static class Sorter implements java.util.Comparator<Sorter.IOrderable> {

        @Override
        public int compare(IOrderable o1, IOrderable o2) {
            if (o1.getOrder() == o2.getOrder()) return 0;
            return o1.getOrder() > o2.getOrder() ? 1 : -1;
        }

        public interface IOrderable {
            int getOrder();
        }
    }

    public static Drawable getDrawable(Context context, TypedArray typedArray, int id, int defId) {
        try {
            int resId = typedArray.getResourceId(id, defId);
            return (resId == -1 ? null : ResourcesCompat.getDrawable(context.getResources(), resId, null));
        } catch(Exception exc) {
            return null;
        }
    }

    public static Drawable getColorDrawable(Context context, TypedArray typedArray, int id, int defId) {
        try {
            if(!typedArray.hasValueOrEmpty(id)) return null;
            int color = typedArray.getColor(id, defId);
            printLog("getColorDrawable : " + color);
            ColorDrawable col = new ColorDrawable();
            col.setColor(color);
            return col;
        } catch(Exception exc) {
            return null;
        }
    }

    public static Drawable getColorOrDrawable(Context context, TypedArray typedArray, @StyleableRes int id, int defId) {
        Drawable background = CpnUtils.getDrawable(context, typedArray, id, -1);
        printLog("drawable : " + background);
        if(background == null) {
            background = CpnUtils.getColorDrawable(context, typedArray, id, -1);
        }

        printLog("colorDrawable : " + background);
        if(background == null) {
            background = ResourcesCompat.getDrawable(context.getResources(), defId, null);
        }

        printLog("getColorOrDrawable : " + background);
        return background;
    }

    public static ColorStateList getColorList(Context context, TypedArray typedArray, @StyleableRes int id, int defColor) {
        ColorStateList colorList = null;
        try {
            colorList = typedArray.getColorStateList(id);
            if(colorList == null) {
//                colorList = ColorStateList.valueOf(defColor);
                colorList = ResourcesCompat.getColorStateList(context.getResources(), defColor, null);
            }
            printLog("getColorList : " + colorList);
        } catch(Exception exc) {
            exc.printStackTrace();
            printLog("Exception caught on getColorList[id : " + id + "] : " + exc.getMessage());
        }

        if(colorList == null) {
            try {
                printLog("try using defColor as single Color");
                colorList = ColorStateList.valueOf(defColor);
            } catch(Exception exc) {
                exc.printStackTrace();
                printLog("Exception caught on getColorList.ColorStateList.valueOf(" + defColor + ")");
            }
        }

        return colorList;
    }

    public static int dpToPx(Context context, int dp) {
        return (int) ((dp * context.getResources().getDisplayMetrics().density) + 0.5);
    }

    public static int pxToDp(Context context, int px) {
        return (int) ((px - 0.5) / context.getResources().getDisplayMetrics().density);
    }
}
